SRC_DIR=	./src/

SRC=		$(SRC_DIR)game.c \
		$(SRC_DIR)is_num.c \
		$(SRC_DIR)main.c \
		$(SRC_DIR)my_getnbr.c \
		$(SRC_DIR)my_putchar.c \
		$(SRC_DIR)my_put_nbr.c \
		$(SRC_DIR)my_putstr.c \
		$(SRC_DIR)my_putstr_color.c \
		$(SRC_DIR)my_strcmp.c \
		$(SRC_DIR)my_strlen.c \
		$(SRC_DIR)readline.c \
		$(SRC_DIR)set_amount.c \
		$(SRC_DIR)set_bet.c \
		$(SRC_DIR)set_is_over.c \
		$(SRC_DIR)set_money.c \
		$(SRC_DIR)set_nb.c

RM=		rm -f

NAME=		my_roulette

OBJS=		$(SRC:.c=.o)

CFLAGS=		-W -Wall -Werror

$(NAME):	$(OBJS)
		cc $(OBJS) -o $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJS)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
