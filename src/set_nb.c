#include <stdlib.h>
#include <time.h>

void	my_putstr_yellow(char *str);
void	my_putchar(char c);
void	my_put_nbr(int nb);

static int	get_random()
{
  int		random;

  srand(time(NULL));
  random = (rand() % 36) + 1;
  return (random);
}

int	set_nb()
{
  int	nb;

  nb = get_random();
  my_putstr_yellow("\nLes jeux sont fait ...\nLe résultat est : ");
  my_put_nbr(nb);
  my_putchar('\n');
  return (nb);
}
