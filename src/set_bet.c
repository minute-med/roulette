#include <stdlib.h>

void	my_putchar(char c);
void	my_putstr(char *str);
void	my_putstr_red(char *str);
char	*readLine();
int	my_getnbr(char *str);
int	is_num(char *str);

int	set_bet()
{
  char	*str;
  int	bet;

  bet = -1;
  while (bet == -1)
    {
      my_putstr("\n--> Sur quel chiffre misez vous ? (1 à 36)\n");
      if ((str = readLine()) == NULL)
	return (-1);
      if (is_num(str) == 1 && (bet = my_getnbr(str)) == -1)
	return (-1);
      if (bet < 1 || bet > 36)
	{
	  my_putchar('\n');
	  my_putstr_red("Error: Vous devez entrer un chiffre entre 1 et 36\n");
	  bet = -1;
	}
      free(str);
    }
  return (bet);
}
