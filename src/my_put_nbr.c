void	my_putchar(char c);
void	my_putstr_yellow(char *str);

void	my_put_nbr(int nb)
{
  if (nb >= 10)
    my_put_nbr(nb / 10);
  my_putchar(nb % 10 + '0');
}

void	my_put_nbr_yellow(int nb)
{
  char	c;
  
  if (nb >= 10)
    my_put_nbr_yellow(nb / 10);
  c = (nb % 10) + '0';
  my_putstr_yellow(&c);
}
