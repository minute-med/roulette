void	my_putstr_yellow(char *str);
void	my_put_nbr_yellow(int nb);
void	my_put_nbr(int nb);
void	my_putchar(char c);

void	set_money(int *money, int amount, int bet, int nb)
{
  if (bet == nb)
    {
      my_putstr_yellow("Vous avez gagné le jackpot. Vos gains augmentent de ");
      my_put_nbr(amount * 3);
      my_putchar('\n');
      *money += (amount * 3);
    }
  else if ((bet % 2) == (nb % 2))
    {
      my_putstr_yellow("Votre mise est de la même couleur. ");
      my_putstr_yellow("Vos gains augmentent de ");
      my_put_nbr(amount / 2);
      my_putchar('\n');
      *money += (amount / 2);
    }
  else
    {
      my_putstr_yellow("Vous n'avez rien gagné. Vous perdez donc votre mise.");
      my_putchar('\n');
      *money -= amount;
    }
  my_putstr_yellow("\nVotre gain actuel est de : ");
  my_put_nbr_yellow(*money);
  my_putstr_yellow(" €\n");
}
