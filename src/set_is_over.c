#include <stdlib.h>

void	my_putstr(char *str);
void	my_putstr_red(char *str);
int	my_getnbr(char *str);
char	*readLine();
int	my_strcmp(char *s1, char *s2);

int	set_is_over(int money)
{
  char	*str;
  int	is_over;

  if (money <= 0)
    return (1);
  is_over = -1;
  while (is_over == -1)
    {
      my_putstr("\nVoulez vous continuer ? (y/n)\n");
      if ((str = readLine()) == NULL)
	return (-1);
      if (my_strcmp(str, "y\n") == 0)
	is_over = 0;
      else if (my_strcmp(str, "n\n") == 0)
	is_over = 1;
      else
	my_putstr_red("\nError : Veuillez entrer 'y' ou 'n'\n");
      free(str);
    }
  return (is_over);
}
