int	my_getnbr(char *str)
{
  int	nb;

  nb = 0;
  while (*str != '\0' && *str >= '0' && *str <= '9')
    {
      nb = (nb * 10) + (*str - 48);
      str += 1;
    }
  return (nb);
}
