#include <stdlib.h>

void	my_putstr(char *str);
char	*readLine();
int	my_getnbr(char *str);
int	is_num(char *str);
void	my_putstr_red(char *str);

int	set_amount(int money)
{
  int	amount;
  char	*str;

  amount = -1;
  while (amount == -1)
    {
      my_putstr("\n--> Quel est le montant de votre mise ?\n");
      if ((str = readLine()) == NULL)
	return (-1);
      if (is_num(str) == 1 && (amount = my_getnbr(str)) == -1)
	    return (-1);
      if (amount > money)
	{
	  my_putstr_red("\nError: Votre mise est supérieur à votre gain\n");
	  amount = -1;
	}
      else if (amount < 1)
	{
	  my_putstr_red("\nError: Veuillez entrer un montant ");
	  my_putstr_red("compris entre 1 et votre gain\n");
	  amount = -1;
	}
      free(str);
    }
  return (amount);
}
