int	set_is_over(int money);
int	set_bet();
void	set_money(int *money, int amount, int bet, int nb);
void	my_putstr_yellow(char *str);
int	set_amount(int money);
int	set_nb();
void	my_put_nbr_yellow(int nb);

static int	game(int *money)
{
  int		amount;
  int		bet;
  int		nb;

  if ((amount = set_amount(*money)) == -1)
    return (-1);
  if ((bet = set_bet()) == -1)
    return (-1);
  if ((nb = set_nb()) == -1)
    return (-1);
  set_money(money, amount, bet, nb);
  return (0);
}

int	loop_game()
{
  int	is_over;
  int	money;

  is_over = 0;
  money = 200;
  my_putstr_yellow("Bienvenu à la roulette ETNA\n");
  my_putstr_yellow("Votre gain actuel est de : ");
  my_put_nbr_yellow(money);
  my_putstr_yellow(" €\n");
  while (is_over != 1)
    {
      if (game(&money) == -1)
	return (-1);
      if ((is_over = set_is_over(money)) == -1)
	return (-1);
    }
  my_putstr_yellow("\nMerci de votre participation à la Roulette de l'ETNA\n");
  return (0);
}
