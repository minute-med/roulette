int	my_strlen(char *str)
{
  int	i;

  for (i = 0; str[i] != '\0'; i += 1);
  return (i);
}
