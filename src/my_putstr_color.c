void	my_putstr(char *str);

void	my_putstr_red(char *str)
{
  my_putstr("\x1B[31m");
  my_putstr(str);
  my_putstr("\x1B[0m");
}

void	my_putstr_yellow(char *str)
{
  my_putstr("\x1B[33m");
  my_putstr(str);
  my_putstr("\x1B[0m");
}
