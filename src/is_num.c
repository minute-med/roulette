int	my_strlen(char *str);

int	is_num(char *str)
{
  if (my_strlen(str) > 6)
    return (0);
  while (*str != '\0' && *str != '\n')
    {
      if (*str < '0' || *str > '9')
	return (0);
      str += 1;
    }
  return (1);
}
